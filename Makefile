PKGS = ngspice sndfile

CFLAGS = -std=c11 -Wall -Wextra -pedantic -O2
LDLIBS += -lm

CPPFLAGS += $(shell pkg-config --cflags-only-I $(PKGS))
CFLAGS += $(shell pkg-config --cflags-only-other $(PKGS))
LDFLAGS += $(shell pkg-config --libs-only-L --libs-only-other $(PKGS))
LDLIBS += $(shell pkg-config --libs-only-l $(PKGS))

.DEFAULT_GOAL = ngspice-wav
