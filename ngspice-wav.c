#define _DEFAULT_SOURCE
#define _POSIX_C_SOURCE 200809L
#include <ctype.h>
#include <math.h>
#include <stdbool.h>  // note: sharedspice.h fails to include this
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sndfile.h>
#include <ngspice/sharedspice.h>

// TODO options:
// * choose initial DC offset as 0 or sample 0
// * input antialiasing on/off
// * output antialiasing on/off
// * multichannel

// note that t = 0 is shifted by one input sample period
// this way we can set the initial DC offset independently

static const double time_epsilon = 1e-9;
static long next_sample;
static bool init_dc_is_zero = false;
static const char* output_value;
static SNDFILE* input_sf, * output_sf;
static double in_fs, out_fs;
static double input_sample_period, output_sample_period;

#define INPUT_HISTORY_SIZE 64
static long input_history_next_sample;
static double input_history[INPUT_HISTORY_SIZE];

static double read_sample(const long sample)
{
    if (sample < input_history_next_sample - INPUT_HISTORY_SIZE)
    {
        fprintf(stderr, "Attempt to read sample %ld when sample window is [%ld,%ld)\n",
            sample, input_history_next_sample - INPUT_HISTORY_SIZE, input_history_next_sample);
        return 0.0;
    }

    while (sample >= input_history_next_sample)
    {
        memcpy(input_history, input_history + INPUT_HISTORY_SIZE / 2, (INPUT_HISTORY_SIZE / 2) * sizeof input_history[0]);
        sf_readf_double(input_sf, input_history + INPUT_HISTORY_SIZE / 2, INPUT_HISTORY_SIZE / 2);
        input_history_next_sample += INPUT_HISTORY_SIZE / 2;
    }

    return input_history[INPUT_HISTORY_SIZE + (sample - input_history_next_sample)];
}

static void write_next_sample(const double value)
{
    const long ret = sf_writef_double(output_sf, &value, 1);
    if (ret < 1) fprintf(stderr, "%s\n", sf_strerror(output_sf));
}

static int handle_SendChar(char* const str, const int id, void* const data)
{
    (void) id;
    (void) data;
    if (strncmp(str, "stderr ", 7) == 0)
    {
        fputs(&str[7], stderr);
        putc('\n', stderr);
    }
    return 0;
}

static int handle_SendStat(char* const status, const int id, void* const data)
{
    (void) id;
    (void) data;
    fputs(status, stderr);
    putc('\n', stderr);
    return 0;
}

static int handle_ControlledExit(const int status, const NG_BOOL unload, const NG_BOOL quit, const int id, void* const data)
{
    (void) unload;
    (void) quit;
    (void) id;
    (void) data;
    exit(status);
}

static int handle_SendInitData(const pvecinfoall infos, const int id, void* const data)
{
    (void) id;
    (void) data;

    fprintf(stderr, "SendInitData %d\n", infos->veccount);

    for (int i = 0; i < infos->veccount; ++i)
    {
        const pvecinfo info = infos->vecs[i];
        fprintf(stderr, "SendInitData %d %s\n", info->number, info->vecname);
    }

    return 0;
}

static int handle_SendData(const pvecvaluesall values, const int num, const int id, void* const data)
{
    (void) num;  // how is this different from values->veccount?
    (void) id;
    (void) data;

    bool do_write = false;

    for (int i = 0; i < values->veccount; ++i)
    {
        const pvecvalues value = values->vecsa[i];
        if (!value->is_scale) continue;
        const long cur_sample = floor((value->creal - input_sample_period + time_epsilon) / output_sample_period);
        if (cur_sample >= next_sample)
        {
            if (cur_sample > next_sample)
                fprintf(stderr, "missed samples %ld through %ld\n", next_sample, cur_sample - 1);

            do_write = true;
            next_sample = cur_sample + 1;
        }
    }

    if (do_write)
    {
        double outval = 0.0;

        for (int i = 0; i < values->veccount; ++i)
        {
            const pvecvalues value = values->vecsa[i];
            if (strcmp(value->name, output_value) != 0) continue;
            outval = value->creal / out_fs;
        }

        write_next_sample(outval);
    }

    return 0;
}

static int handle_GetVSRCData(double *const voltage, const double actual_time, char* const node_name, const int id, void* const data)
{
    (void) node_name;
    (void) id;
    (void) data;
    if (actual_time == 0.0)
    {
        if (init_dc_is_zero)
            *voltage = 0.0;
        else
            *voltage = in_fs * read_sample(0);
    }
    else
    {
        const double sample_time = actual_time - input_sample_period;
        *voltage = in_fs * read_sample((long) floor((sample_time + 2*time_epsilon) / input_sample_period));
    }
    return 0;
}

static int handle_GetISRCData(double *const current, const double actual_time, char* const node_name, const int id, void* const data)
{
    (void) node_name;
    (void) id;
    (void) data;
    if (actual_time == 0.0)
    {
        if (init_dc_is_zero)
            *current = 0.0;
        else
            *current = in_fs * read_sample(0);
    }
    else
    {
        const double sample_time = actual_time - input_sample_period;
        *current = in_fs * read_sample((long) floor((sample_time + 2*time_epsilon) / input_sample_period));
    }
    return 0;
}

static int handle_GetSyncData(const double actual_time, double* const delta_time, const double old_delta_time, const int redostep, const int id, const int loc, void* const data)
{
    (void) old_delta_time;
    (void) redostep;
    (void) id;
    (void) data;
    //fprintf(stderr, "GetSyncData %g %g %g %d %d\n", actual_time, *delta_time, old_delta_time, redostep, loc);
    // FIXME: what to do if loc == 1??
    if (loc == 0 && actual_time + *delta_time - input_sample_period > output_sample_period * next_sample)
    {
        *delta_time = output_sample_period * next_sample + input_sample_period - actual_time;
    }
    return 0;
}

struct param
{
    const char* param;
    const char* value;
};

static int set_spice_param(const struct param param)
{
    char* source_line;
    size_t source_line_size;
    FILE* const source_line_file = open_memstream(&source_line, &source_line_size);
    fprintf(source_line_file, "alterparam %s = {%s}\n", param.param, param.value);
    fclose(source_line_file);
    const int ret = ngSpice_Command(source_line);
    free(source_line);
    return ret;
}

static int load_spice_model(const char* const file)
{
    char* source_line;
    size_t source_line_size;
    FILE* const source_line_file = open_memstream(&source_line, &source_line_size);
    fprintf(source_line_file, "source %s\n", file);
    fclose(source_line_file);
    const int ret = ngSpice_Command(source_line);
    free(source_line);
    return ret;
}

static int run_spice_tran(const double step, const double stop, const double start, const double max)
{
    char* tran_line;
    size_t tran_line_size;
    FILE* const tran_line_file = open_memstream(&tran_line, &tran_line_size);
    fprintf(tran_line_file, "tran %g %g %g %g\n", step, stop, start, max);
    fclose(tran_line_file);
    const int ret = ngSpice_Command(tran_line);
    free(tran_line);
    return ret;
}

static void usage(FILE* const file, const char* const argv0)
{
    fprintf(file, "Usage: %s [options] <SPICE model> <input WAV> <output value> <output WAV>\n", argv0);
    fprintf(file, "\n");
    fprintf(file, "  -l <input full-scale value, V or A>\n");
    fprintf(file, "  -L <output full-scale value, V or A>\n");
    fprintf(file, "  -o <oversampling factor>\n");
    fprintf(file, "  -p <param>=<value>\n");
    fprintf(file, "  -R <output sampling rate, Hz>\n");
    fprintf(file, "  -t <tail time, s>\n");
}

int main(int argc, char* argv[])
{
    // parse options
    long output_rate = 48000;
    double oversample = 8.0;
    double tail_time = 0.0;
    struct param* params = NULL;
    int nparams = 0;

    int opt;
    while ((opt = getopt(argc, argv, "hl:L:o:p:R:t:")) != -1)
    {
        switch (opt)
        {
        case 'h':
            usage(stdout, argv[0]);
            exit(EXIT_SUCCESS);

        case 'l':
            in_fs = atof(optarg);
            break;

        case 'L':
            out_fs = atof(optarg);
            break;

        case 'o':
            oversample = atof(optarg);
            break;

        case 'p':
        {
            char* const param = strtok(optarg, "=");
            char* const value = strtok(NULL, "");
            if (value == NULL)
            {
                fprintf(stderr, "-p missing value\n\n");
                usage(stderr, argv[0]);
                exit(EXIT_FAILURE);
            }

            // parameters are converted to lowercase by sharedspice...
            for (char* p = param; *p != '\0'; ++p)
                *p = tolower(*p);

            ++nparams;
            params = reallocarray(params, nparams, sizeof *params);
            params[nparams - 1].param = param;
            params[nparams - 1].value = value;

            break;
        }

        case 'R':
            output_rate = atol(optarg);
            break;

        case 't':
            tail_time = atof(optarg);
            break;

        default:
            fprintf(stderr, "Unknown option -%c\n\n", optopt);
            usage(stderr, argv[0]);
            exit(EXIT_FAILURE);
        }
    }

    if (argc - optind < 4)
    {
        usage(stderr, argv[0]);
        exit(EXIT_FAILURE);
    }

    const char* const spice_model = argv[optind + 0];
    const char* const input_wave = argv[optind + 1];
    output_value = argv[optind + 2];
    const char* const output_wave = argv[optind + 3];

    // initialize spice
    ngSpice_Init(handle_SendChar, handle_SendStat, handle_ControlledExit, handle_SendData, handle_SendInitData, NULL, NULL);
    ngSpice_Init_Sync(handle_GetVSRCData, handle_GetISRCData, handle_GetSyncData, NULL, NULL);

    // load spice model
    load_spice_model(spice_model);

    // set model parameters
    for (int i = 0; i < nparams; ++i)
        set_spice_param(params[i]);

    ngSpice_Command("reset");

    // open input wave file
    SF_INFO input_sfi = { .format = 0 };
    input_sf = sf_open(input_wave, SFM_READ, &input_sfi);
    const long num_input_samples = input_sfi.frames;
    input_history_next_sample = 0;
    input_sample_period = 1.0 / input_sfi.samplerate;

    // open output wave file
    SF_INFO output_sfi = { .format = SF_FORMAT_WAV | SF_FORMAT_FLOAT, .samplerate = output_rate, .channels = 1 };
    output_sf = sf_open(output_wave, SFM_WRITE, &output_sfi);
    output_sample_period = 1.0 / output_rate;

    // run simulation
    next_sample = 0;
    run_spice_tran(output_sample_period, (num_input_samples + 0.5) * input_sample_period + tail_time, 0.5 * input_sample_period, fmin(input_sample_period, output_sample_period) / oversample);

    // clean up
    sf_close(input_sf);
    sf_close(output_sf);

    return EXIT_SUCCESS;
}
